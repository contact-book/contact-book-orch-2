import http from 'http';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import errorHandler from 'errorhandler';

import HomeController from './controllers/HomeController';
import UsersController from './controllers/UsersController';
import GreetingController from './controllers/GreetingController';

const app = express();
app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());
app.use(cors());

app.use('/users', UsersController.index);
app.use('/greeting/:name', GreetingController.index);
app.use('/', HomeController.index);

if (app.get('env') === 'development') {
    app.use(errorHandler());
}

const server = http.createServer(app);
server.listen(app.get('port'), () => {
    console.info('Express server listening on port ' + app.get('port'));
});
