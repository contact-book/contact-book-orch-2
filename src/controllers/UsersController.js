import Users from '../models/Users';

export default {
    index: async (req, res) => {
        let allUsers;

        try {
            allUsers = await Users.all();
        } catch (err) {
            console.error(err);
        }

        res.json(allUsers);
    }
};
