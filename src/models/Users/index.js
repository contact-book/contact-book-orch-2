import Fetcher from '../Fetcher';

export default {
    all: () => {
        Fetcher('get', 'http://localhost:8882/api/users/')
            .then(response => {
                return response.data;
            })
            .catch(err => {
                console.error(err);
            });
    }
}
