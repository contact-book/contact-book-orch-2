import axios from 'axios';

export default (method, url, data) =>
    axios.request({
        method,
        url,
        data,
        auth: {
            username: 'usernamez',
            password: 'passwordinBase64'
        }
    })
    .then(response => {
        return response;
    })
    .catch(err => {
        console.error(err);
        throw new Error(err);
    });
